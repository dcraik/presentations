\documentclass{beamer}

\graphicspath{{./figs/}}
\usepackage{tikz}
\usetikzlibrary{patterns,shapes,arrows,shadings}

\usepackage{amsmath} % Adds a large collection of math symbols
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{upgreek} % Adds in support for greek letters in roman typeset
\usepackage{pifont}
\usepackage{ifthen}
\usepackage{xspace}
\usepackage{fancyvrb}
\usepackage{framed}
\usepackage{textcmds}
\usepackage{dsfont}
\usepackage{rotating}

\input{lhcb-symbols-def}

\usetheme{CambridgeUS}
\usecolortheme{mit}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc}

\title[Charm tagging \& Higgs]{Prospects for charm tagging and the Higgs}
\subtitle{HL/HE-LHC Meeting}
\author[Dan Craik]{Daniel~Craik\\ on behalf of the LHCb colaboration}
\institute[MIT]{Massachusetts Institute of Technology}
\date[06/04/2018]{6th April, 2018}
\titlegraphic{\hspace*{\fill} \includegraphics[height=0.9cm]{lhcb}\hfill\includegraphics[height=0.9cm]{mit}\hfill\includegraphics[height=0.9cm]{esu}\hspace*{\fill}
	%\\\vspace{1em}\hspace*{\fill}\includegraphics[width=\textwidth]{fermilab}\hspace*{\fill}
}
\setbeamertemplate{headline}{}
\setbeamertemplate{navigation symbols}{}{}
\setbeamertemplate{caption}{%
    \begin{beamercolorbox}[wd=.8\textwidth, sep=.2ex]{block body}\insertcaption%
    \end{beamercolorbox}
}

\definecolor{dgreen}{rgb}{0,0.7,0}

\begin{document}

{
\setbeamertemplate{footline}{
	\vspace{-3.5em}\hspace*{\fill}\includegraphics[width=\textwidth]{fermilab}\hspace*{\fill}
}
\begin{frame}
    \titlepage
\end{frame}
}
\addtocounter{framenumber}{-1}

\begin{frame}{Overview}
	\begin{itemize}
		\item Observation of a Higgs boson at $125\gevcc$ standout achievement of LHC Run I
		\item Gauge sector seems to be SM-like
		\item Less known about fermions
		\item Thus far, only third-generation decays observed
		\item $\decay{\Hz}{\cquark\cquarkbar}$ has largest SM BF of second-generation decays
		\item $\sim30\times$ suppressed \cf $\bquark\bquarkbar$
	\end{itemize}
	\begin{center}
		\includegraphics[width=0.4\textwidth]{higgsBRs2}%From https://arxiv.org/pdf/1704.00790.pdf
		\includegraphics[width=0.4\textwidth]{higgsCouplings}%From https://arxiv.org/pdf/1606.02266.pdf
	\end{center}
\end{frame}

\begin{frame}{\boldmath \decay{\Hz}{\ccbar} @ Atlas}
	\begin{itemize}
		\item Atlas recently performed a search for $\decay{\Hz}{\ccbar}$ produced through $Z+H$
		\item Used $36.1\invfb$ of data at $13\tev$ 
		\item Limit set is $\sim100\times$ SM prediction
	\end{itemize}
	\begin{center}
		\includegraphics[width=0.8\textwidth]{H2ccbarAtlas}%From https://arxiv.org/pdf/1802.04329.pdf
	\end{center}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,remember picture, overlay,shift={(current page.north east)}]
		\node [white, rounded rectangle, fill=mitgray, anchor=north east, align=center, minimum width=0.28\textwidth] at (-0.10,-0.06) {\tiny \href{https://arxiv.org/abs/1802.04329}{arXiv:1802.04329}};
	\end{tikzpicture}
\end{frame}

\begin{frame}{Higgs searches @ LHCb}
	\color{mitred} {\bf Disadvantages}
	\begin{itemize}
		\item Lower luminosity
		\item Smaller acceptance
		\item Non-hermetic
	\end{itemize}
	{\bf Advantages}
	\begin{itemize}
		\item Low pileup
		\item Excellent secondary vertex reconstruction
		\item Complementary coverage
	\end{itemize}
	{\bf Focus on $\bquark$, $\cquark$ and $\tau$ channels}
\end{frame}

\begin{frame}{\boldmath Higgs searches @ LHCb: $\tautau$}
	\begin{minipage}{0.64\textwidth}
	\begin{itemize}
		\item Limits set on $\decay{\Hz}{\tautau}$ in forward region as a function of Higgs mass
		\item Analysis used $1\invfb$ of data at $7\tev$
		\item $\tau$ decays to muon, electron and hadronic final states considered
		\item No requirements on $\Hz$ production mechanism
			\begin{itemize}
				\item Trigger on $\tau$ decays
			\end{itemize}
		\item Limit $\sim100\times$ SM prediction
		\item Also set limits on $\tan\beta$ in MSSM
	\end{itemize}
	\end{minipage}
	\begin{minipage}{0.35\textwidth}
	\includegraphics[width=\textwidth]{H2tautauA}\\%From PAPER-2013-009
	\includegraphics[width=\textwidth]{H2tautauB}%From PAPER-2013-009
	\end{minipage}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,remember picture, overlay,shift={(current page.north east)}]
		\node [white, rounded rectangle, fill=mitgray, anchor=north east, align=center, minimum width=0.28\textwidth] at (-0.10,-0.06) {\tiny \href{http://dx.doi.org/10.1007/JHEP05(2013)132}{JHEP {\bf 05} (2013) 132}};
	\end{tikzpicture}
\end{frame}

\begin{frame}{\boldmath Higgs searches @ LHCb: $\bquark\bquarkbar$ and $\cquark\cquarkbar$}
	\begin{minipage}{0.35\textwidth}
	\includegraphics[width=\textwidth]{H2bbbar}\\%From CONF-2016-006
	\includegraphics[width=\textwidth]{H2ccbar}%From CONF-2016-006
	\end{minipage}
	\begin{minipage}{0.64\textwidth}
	\begin{itemize}
		\item Also studied $\bbbar$ and $\ccbar$ final states
		\item Analyses used $2\invfb$ of data at $8\tev$
		\item Use $VH$ associated production
		\item Trigger on the vector boson
%		\item Limit on $\decay{\Hz}{\bbbar}$ ($\decay{\Hz}{\ccbar}$) is $50\times$ ($6400\times$) SM prediction
		\item Upper limits on Yukawa couplings of $y^\bquark<7y^\bquark_{\rm SM}$ and $y^\cquark<80y^\cquark_{\rm SM}$
		\item How much better can we do after upgrades?
	\end{itemize}
	\end{minipage}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,remember picture, overlay,shift={(current page.north east)}]
		\node [white, rounded rectangle, fill=mitgray, anchor=north east, align=center, minimum width=0.28\textwidth] at (-0.10,-0.16) {\tiny \href{http://inspirehep.net/record/1487345}{LHCb-CONF-2016-006}};
	\end{tikzpicture}
\end{frame}

\begin{frame}{LHCb upgrade timeline}
	\begin{center}
		\begin{tikzpicture}[x=\textwidth,y=\textwidth]
			\fill[mitred] (0.00,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.05,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.10,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.15,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.20,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.25,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.30,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.35,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.40,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.45,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.50,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.55,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.60,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.65,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.70,0.) rectangle ++(0.048,0.02) {};
			\fill[mitred] (0.75,0.) rectangle ++(0.048,0.02) {};
			\draw[thick,->] (0,0) -- (0.8,0);
			\foreach \x in {0,0.1,0.2,0.3,0.4,0.5,0.6,0.7}
			      \draw (\x,3pt) -- (\x,-3pt);
			\draw (0.00,0) node[below=3pt] (a) {\tiny now} node[above=3pt] {};
			\draw (0.10,0) node[below=3pt] {\tiny 2020} node[above=3pt] {};
			\draw (0.20,0) node[below=3pt] {\tiny 2022} node[above=3pt] {};
			\draw (0.30,0) node[below=3pt] {\tiny 2024} node[above=3pt] {};
			\draw (0.40,0) node[below=3pt] {\tiny 2026} node[above=3pt] {};
			\draw (0.50,0) node[below=3pt] {\tiny 2028} node[above=3pt] {};
			\draw (0.60,0) node[below=3pt] {\tiny 2030} node[above=3pt] {};
			\draw (0.70,0) node[below=3pt] {\tiny 2032} node[above=3pt] {};
			\node[white,fill=blue!80!gray,rectangle,minimum width=0.299\textwidth,minimum height=0.04\textwidth] at (0.149,0.05) {LHC};
			\node[white,fill=blue!40!gray,rectangle,minimum width=0.499\textwidth,minimum height=0.04\textwidth] at (0.549,0.05) {HL-LHC};
			\fill[mitgray]  (0.00,0.08) rectangle ++(0.048,0.04) {};
			\fill[darkgray] (0.05,0.08) rectangle ++(0.098,0.04) {};
			\fill[mitgray]  (0.15,0.08) rectangle ++(0.148,0.04) {};
			\fill[darkgray] (0.30,0.08) rectangle ++(0.148,0.04) {};
			\fill[mitgray]  (0.45,0.08) rectangle ++(0.148,0.04) {};
			\fill[darkgray] (0.60,0.08) rectangle ++(0.048,0.04) {};
			\fill[mitgray]  (0.65,0.08) rectangle ++(0.148,0.04) {};
			\draw (0.025,0.08) node[above=12pt] {\scriptsize Run II};
			\draw (0.100,0.08) node[above=12pt] {\scriptsize LS 2};
			\draw (0.225,0.08) node[above=12pt] {\scriptsize Run III};
			\draw (0.375,0.08) node[above=12pt] {\scriptsize LS 3};
			\draw (0.525,0.08) node[above=12pt] {\scriptsize Run IV};
			\draw (0.625,0.08) node[above=12pt] {\scriptsize LS 4};
			\draw (0.725,0.08) node[above=12pt] {\scriptsize Runs V+};
			\draw[->,thick] (0.100,-0.10) -- (0.100,-0.05);
			\draw[->,thick] (0.375,-0.10) -- (0.375,-0.05);
			\draw[->,thick] (0.625,-0.10) -- (0.625,-0.05);
			\node[] at (0.100,-0.12) {\tiny Phase I Upgrade};
			\node[] at (0.100,-0.14) {\tiny Triggerless readout at 40\mhz};
			\node[] at (0.100,-0.16) {\tiny New VELO and tracking};
			\node[] at (0.375,-0.12) {\tiny Phase Ib Upgrade};
			\node[] at (0.375,-0.14) {\tiny Possible stepping stone};
			\node[] at (0.625,-0.12) {\tiny Phase II Upgrade};
			\node[] at (0.625,-0.14) {\tiny Upgrade for HL};
			\node[] at (0.625,-0.16) {\tiny New ECAL};
			\node[] at (0.625,-0.18) {\tiny Shielding for muon system};
			\draw[->]  (0.000,-0.20) -- (0.095,-0.20);
			\draw[<->] (0.105,-0.20) -- (0.620,-0.20);
			\draw[<-]  (0.630,-0.20) -- (0.800,-0.20);
			\foreach \x in {0.100,0.625}
			      \draw (\x,-0.19) -- (\x,-0.21);
			\node[] at (0.0500,-0.22) {\tiny $8\invfb$};
			\node[] at (0.3625,-0.22) {\tiny $50\invfb$};
			\node[] at (0.7175,-0.22) {\tiny $300\invfb$};
			%\draw[<->,blue] (0.055,-0.22) -- (0.345,-0.22);
			%\foreach \x in {0.050,0.350}
			%\draw[blue] (\x,-0.21) -- (\x,-0.23);
			%\node[blue] at (0.200,-0.25) {\tiny Belle 2};
			%\node[blue] at (0.200,-0.27) {\tiny $50\invab$};
			%\draw[<-] (0.355,-0.22) -- (0.800,-0.22);
			%\node[] at (0.575,-0.25) {\tiny LHCb may be only dedicated $B$-physics experiment};
		\end{tikzpicture}
	\end{center}
\end{frame}

\begin{frame}{The LHCb detector: phase 1 upgrade}
	\begin{center}
	\begin{minipage}{0.64\textwidth}
		\vspace{2em}
		\includegraphics[width=\textwidth]{detectorPhase1}
	\end{minipage}
	\begin{minipage}{0.34\textwidth}
		\begin{itemize}
			\item Triggerless readout at $40\mhz$
			\item New vertex locator
			\item New tracking (UT, SciFi)
		\end{itemize}
	\end{minipage}
	\end{center}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,remember picture, overlay,shift={(current page.north east)}]
		\node [white, rounded rectangle, fill=mitgray, anchor=north east, align=center, minimum width=0.28\textwidth] at (-0.10,-0.14) {\tiny \href{https://cds.cern.ch/record/1443882}{CERN-LHCC-2012-007}};
	\end{tikzpicture}
\end{frame}

\begin{frame}{The LHCb detector: phase 2 upgrade}
	\begin{center}
	\begin{minipage}{0.64\textwidth}
		\includegraphics[width=\textwidth]{detectorPhase2}
	\end{minipage}
	\begin{minipage}{0.34\textwidth}
		\begin{itemize}
			\item Tracking in magnet
			\item ECAL upgrade
			\item TORCH for PID or ToF
			\item Replace HCAL with shielding
			\item Some changes could happen as part of phase Ib
		\end{itemize}
	\end{minipage}
	\end{center}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,remember picture, overlay,shift={(current page.north east)}]
		\node [white, rounded rectangle, fill=mitgray, anchor=north east, align=center, minimum width=0.28\textwidth] at (-0.10,-0.14) {\tiny \href{https://cds.cern.ch/record/2244311}{CERN-LHCC-2017-003}};
	\end{tikzpicture}
\end{frame}

\begin{frame}{Charm tagging @ LHCb}
	\begin{minipage}{0.5\textwidth}
	\begin{itemize}
		\item Charm tagging non-trivial
		\item Charm has long lifetime (displaced vertex/muon or exclusive $D$)
		\item But so does beauty...
		\item Distinguish using features of SV
		\item Need to calibrate using data...
	\end{itemize}
	\end{minipage}
	\begin{minipage}{0.24\textwidth}
		\begin{center}
			\begin{tikzpicture}[x=\textwidth,y=\textwidth]
				%jet1
				\draw[->,>=stealth] (0.,1.8) -- ( 0.36,-0.01) {};
				\draw[->,>=stealth] (0.,1.8) -- ( 0.24, 0.00) {};
				\draw[->,>=stealth] (0.,1.8) -- (-0.32,-0.02) {};
				\draw[->,>=stealth] (0.,1.8) -- (-0.26, 0.01) {};
				\draw[->,>=stealth] (0.,1.8) -- (-0.06, 0.03) {};
				%tag SV
				\draw[dashed,thick,red] (0.,1.8) -- (0.,0.8) {};
				\draw[thick,red,->,>=stealth] (0.,0.8) -- (-0.11,0.01) {};
				\draw[thick,red,->,>=stealth] (0.,0.8) -- (0.13,0.01) {};
				\draw[thick,red,->,>=stealth] (0.,0.8) -- (0.1,-0.02) {};
				%text
				\node[] at (0.37,-0.1) {\tiny Charm jet};
				\node[red] at (0.50,0.9) {\tiny Displaced SV};
				\node[] at (0.25,1.8) {\tiny Primary vertex};
				%cone
				\fill[
				  top color=gray!50,
				  bottom color=gray!10,
				  shading=axis,
				  opacity=0.25
				  ] 
				  (0,0) ellipse (0.4 and 0.05);
				\fill[
				  left color=gray!50!black,
				  right color=gray!50!black,
				  middle color=gray!50,
				  shading=axis,
				  opacity=0.25
				  ] 
				  (0.4,0) -- (0,1.8) -- (-0.4,0) arc (180:360:0.4 and 0.05);
				\draw 
				  (-0.4,0) arc (180:360:0.4 and 0.05) -- (0,1.8) -- cycle;
				\draw[dashed]
				  (-0.4,0) arc (180:0:0.4 and 0.05);
			\end{tikzpicture}
		\end{center}
	\end{minipage}
	\begin{minipage}{0.24\textwidth}
		\begin{center}
			\begin{tikzpicture}[x=\textwidth,y=\textwidth]
				%jet1
				\draw[->,>=stealth] (0.,1.8) -- ( 0.36,-0.01) {};
				\draw[->,>=stealth] (0.,1.8) -- ( 0.24, 0.00) {};
				\draw[->,>=stealth] (0.,1.8) -- (-0.32,-0.02) {};
				\draw[->,>=stealth] (0.,1.8) -- (-0.23, 0.01) {};
				\draw[->,>=stealth] (0.,1.8) -- (-0.06, 0.03) {};
				%tag SV
				\draw[dashed,thick,green!60!black] (0.,1.8) -- (0.,0.8) {};
				\draw[dashed,thick,green!60!black,->,>=stealth] (0.,0.8) -- (-0.11,0.01) {};
				\draw[dashed,thick,green!60!black,->,>=stealth] (0.,0.8) -- (0.13,0.01) {};
				\draw[thick,green!60!black,->,>=stealth] (0.,0.8) -- (0.1,-0.02) {};
				%text
				\node[] at (0.37,-0.1) {\tiny Charm jet};
				\node[green!60!black] at (0.40,0.9) {\tiny $\mun$};
				\node[] at (0.25,1.8) {\tiny Primary vertex};
				%cone
				\fill[
				  top color=gray!50,
				  bottom color=gray!10,
				  shading=axis,
				  opacity=0.25
				  ] 
				  (0,0) ellipse (0.4 and 0.05);
				\fill[
				  left color=gray!50!black,
				  right color=gray!50!black,
				  middle color=gray!50,
				  shading=axis,
				  opacity=0.25
				  ] 
				  (0.4,0) -- (0,1.8) -- (-0.4,0) arc (180:360:0.4 and 0.05);
				\draw 
				  (-0.4,0) arc (180:360:0.4 and 0.05) -- (0,1.8) -- cycle;
				\draw[dashed]
				  (-0.4,0) arc (180:0:0.4 and 0.05);
			\end{tikzpicture}
		\end{center}
	\end{minipage}
\end{frame}

\begin{frame}{Charm tagging @ LHCb}
	\begin{center}
	\begin{minipage}{0.40\textwidth}
		\begin{itemize}
			\item BDTs developed to tag jets in Run 1 data
			%\item Trained on simulated datasets (top)
			\item Efficiency determined on flavour-enriched samples
				\begin{itemize}
					\item \eg\ tagged by fully reconstructed (middle) $B$ or (bottom) $D$ decays on ``other'' jet
				\end{itemize}
			\item 2D fit to corrected mass and track multiplicity of reconstructed secondary vertices also gives good separation of jet flavours
		\end{itemize}
	\end{minipage}
	\begin{minipage}{0.59\textwidth}
	\includegraphics[width=0.98\textwidth]{jetTagBDT}\\
	%\includegraphics[width=0.3\textwidth]{jetTagBDTb}
	%\includegraphics[width=0.3\textwidth]{jetTagBDTc}
	%\includegraphics[width=0.3\textwidth]{jetTagBDTl}\\
	\includegraphics[width=0.49\textwidth]{jetTagSVMCorb}
	\hspace*{\fill}
	\includegraphics[width=0.49\textwidth]{jetTagSVNb}\\
	\includegraphics[width=0.49\textwidth]{jetTagSVMCorc}
	\hspace*{\fill}
	\includegraphics[width=0.49\textwidth]{jetTagSVNc}
	\end{minipage}
	\end{center}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,overlay,shift={(current page.north east)}]
		\node [white, rounded rectangle, fill=mitgray, anchor=north east, align=center, minimum width=0.28\textwidth] at (-0.10,-0.06) {\tiny \href{https://doi.org/10.1088/1748-0221/10/06/P06013}{JINST {\bf 10} (2015) P06013}};
	\end{tikzpicture}
\end{frame}

\begin{frame}{Charm tagging @ LHCb: future}
	\color{mitred} {\bf Run II}
	\begin{itemize}
		\item Jet tagging efficiency studies underway on 13\tev dataset
		\item Unlike in Run I, these benefit from dedicated calibration samples
		\item New 13\tev jet studies to follow
	\end{itemize}
	{\bf Beyond}
	\begin{itemize}
		\item Need to handle pileup
		\item Displaced vertex tagging will benefit from VELO/tracking upgrades
		\item Tagging efficiency expected to be maintained or improved
	\end{itemize}
\end{frame}

\begin{frame}{Charm tagging @ LHCb: future}
	\begin{minipage}{0.59\textwidth}
		\begin{center}
		\includegraphics[width=\textwidth]{jetTagEff}
		\end{center}
	\end{minipage}
	\begin{minipage}{0.40\textwidth}
	\begin{itemize}
		\item {\bf\color{blue} Run I}
		\item Expectations for {\bf\color{dgreen} Run III}, {\bf\color{red} Phase II} {\bf\color{cyan} Upgrade} options.
		\item {\bf Perfect detector} and {\bf \color{gray} perfect with reconstruction efficiency}
		\item Dashed lines have lower $\chi^2_{\rm IP}$ requirement
	\end{itemize}
	\end{minipage}
	\begin{itemize}
		\item Requiring a two-body SV limits $\cquark$-jet efficiency to $\sim55\,\%$
		\item Can boost dijet efficiency by only requiring a single jet to pass tight $\cquark$-tagging requirements
	\end{itemize}
\end{frame}

\begin{frame}{\boldmath $\decay{\Hz}{\cquark\cquarkbar}$ @ LHCb: future}
	\color{mitred} {\bf Prospects for upgrade phase 2}
	\begin{itemize}
		\item $VH$ cross-section in LHCb acceptance increases by a factor of $\sim7$ from $8\tev$ to $14\tev$
		\item After $300\invfb$, expect to set limit on Yukawa coupling of $\sim7y^\cquark_{\rm SM}$
		\item With improvements to detector performance and $\bquark-\cquark$ separation, and looser tagging requirements, this could be brought to $\sim2y^\cquark_{\rm SM}$
		\item If VBF production can also be utilised, this could yield similar statistics
	\end{itemize}
\end{frame}

\begin{frame}{Summary}
	\begin{itemize}
		\item Charm tagging performance at LHCb expected to be maintained or improved in the HL-LHC era
		\item With modest improvements, SM $\decay{\Hz}{\ccbar}$ may be within reach
		\item On the same timescale, LHCb should provide the first observation of $VH(\bbbar)$ in the forward region
	\end{itemize}
\end{frame}

\end{document}
