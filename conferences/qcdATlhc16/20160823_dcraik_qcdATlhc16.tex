% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode


\documentclass{beamer}

\graphicspath{{./figs/}}
\usepackage{tikz}
\usetikzlibrary{patterns, shapes.geometric, shapes.misc, positioning}

\usepackage{amsmath} % Adds a large collection of math symbols
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{upgreek} % Adds in support for greek letters in roman typeset
\usepackage{pifont}
\usepackage{ifthen}
\usepackage{xspace}
\usepackage{multirow}

\input{lhcb-symbols-def}

\mode<presentation>
{
  \usetheme{Warsaw}
  % or ...

  \setbeamercovered{transparent}
  % or whatever (possibly just delete it)
}


\usepackage[english]{babel}
% or whatever

\usepackage[utf8]{inputenc}
% or whatever

\usepackage{times}
\usepackage[T1]{fontenc}
% Or whatever. Note that the encoding and the font should match. If T1
% does not look nice, try deleting the line with the fontenc.

\newcommand{\samelineand}{\qquad}

\title{Heavy Quark Spectroscopy at LHCb}

%\subtitle
%{Beauty 2016}

\author[Daniel~Craik]{Daniel~Craik}


\institute[University of Edinburgh]{University of Edinburgh}

\titlegraphic{
  %\hspace*{\fill}
  \includegraphics[height=0.9cm]{edinburgh}\hfill\includegraphics[height=0.9cm]{lhcb}\\%\hfill\includegraphics[height=0.9cm]{beauty}\\ %\includegraphics[width=\textwidth]{marseille}
  \hspace*{\fill}\includegraphics[height=2.4cm]{qcdATlhc}\hspace*{\fill}
  %\includegraphics[height=0.9cm]{lhcb}\hfill\includegraphics[height=0.9cm]{edinburgh}\\\includegraphics[width=\textwidth]{marseille}
%	\begin{tikzpicture}[x=\textwidth,y=\textwidth]
%		\node[inner sep=0pt, anchor=south west] at (0, 0.1)
%		{\includegraphics[height=0.9cm]{lhcb}};
%		\node[inner sep=0pt, anchor=south west] at (0, 0)
%		{\includegraphics[height=0.9cm]{edinburgh}};
%		\node[inner sep=0pt, anchor=south east] at (1.0, 0)
%		{\includegraphics[height=2.4cm]{beauty}};
%		\node[inner sep=0pt, anchor=south west] at (-0.1, 0)
%		{\includegraphics[width=\textwidth]{marseille}};
%		\node[white, anchor=east] at (0.9,0.08) {\small Beauty 2016};
%		\node[white, anchor=east] at (0.9,0.055) {\small Marseille};
%		\node[white, anchor=east] at (0.9,0.02) {\small 2nd - 6th May, 2016};
%		%\draw [step=0.025,gray, very thin] (-0.1,0.) grid (0.9,0.175);
%	\end{tikzpicture}
  %\hspace*{\fill}
}

\date[23/08/2016]{23rd August, 2016\\\vspace{1em}on behalf of the LHCb Collaboration}

\setbeamertemplate{navigation symbols}{}{%
%    \usebeamerfont{footline}%
%    \usebeamercolor[fg]{footline}%
%    \hspace{1em}%
%    \insertframenumber
}

\setbeamertemplate{caption}{%
    \begin{beamercolorbox}[wd=.8\textwidth, sep=.2ex]{block body}\insertcaption%
    \end{beamercolorbox}
}

\definecolor{orange}{rgb}{1,0.5,0}
\definecolor{dred}{rgb}{0.7,0,0}
\definecolor{dmagenta}{rgb}{0.7,0,0.7}
\definecolor{dgreen}{rgb}{0,0.7,0}
\definecolor{dblue}{rgb}{0,0,0.7}
\definecolor{cyan}{rgb}{0,0.7,0.7}
\definecolor{teal}{rgb}{0,0.4,0.7}

\newcommand{\boldblack}{\boldmath \color{black} \bf}
\newcommand{\boldred}{\boldmath \color{red} \bf}
\newcommand{\boldblue}{\boldmath \color{blue} \bf}
\newcommand{\boldgreen}{\boldmath \color{dgreen} \bf}
\newcommand{\boldmagenta}{\boldmath \color{magenta} \bf}
\newcommand{\boldcyan}{\boldmath \color{cyan} \bf}

\begin{document}

\setbeamertemplate{footline}{
%\includegraphics[height=1.5cm]{marseille}
}

\begin{frame}
  \titlepage
\end{frame}

\setbeamertemplate{footline}{\hspace*{\fill}\insertshortdate\hfill\insertshortauthor\hfill\insertshorttitle\hfill\insertframenumber\hspace*{\fill}\vspace{1em}}
\addtocounter{framenumber}{-1}

\begin{frame}{Outline}
	\begin{itemize}
		\item Wide range of heavy quark spectroscopy analyses performed at LHCb
		\item Talk will focus on most recent results
		\begin{itemize}
			\item $\Ds$ spectroscopy in inclusive $\Dstar K$
			\item $\Dz$ spectroscopy from $\Bm\to\Dp\pim\pim$ decays
			\item Characterisation of the $\Xi_b^{*0}$ resonances
			\item Observation of $\eta_c(2S)\to\proton\antiproton$ decays
			%\item Light spectroscopy in $\Dz\to\KS\Kp\pim$
		\end{itemize}
		\item All results based on analyses of full Run I dataset
		\item Exotics covered in a separate talk (Giovanni --- up next)
	\end{itemize}
\end{frame}

\begin{frame}{The LHCb Detector}
	\begin{center}
	\begin{minipage}{0.64\textwidth}
		\includegraphics[width=\textwidth]{detector}
	\end{minipage}
	\begin{minipage}{0.34\textwidth}
		\begin{itemize}
			\item Instrumentation in the forward region ($2 < \eta < 5$)
			\item Excellent vertex reconstruction
			\item Precise tracking before and after magnet
			\item Good PID separation up to $\sim100\gevc$
		\end{itemize}
	\end{minipage}
	\end{center}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,overlay,shift={(current page.south west)}]
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth] at (0.60,0.00) {\tiny Int. J. Mod. Phys. A 30 (2015) 1530022};
	\end{tikzpicture}
\end{frame}

\begin{frame}{$\Ds$ Spectroscopy}
	\begin{center}
	\begin{minipage}{0.49\textwidth}
		\begin{itemize}
			\item Meson spectroscopy tests refine models of QCD
			\item $\Ds$ mesons particularly interesting with one heavy and one light quark
			\item Unexpected large mass splitting seen between the {\color{dgreen} 1P} states
			\item Two states recently observed by LHCb considered two of the four {\color{magenta} 1D} states
			\item At least three more states expected up to 3\gevcc
			\begin{itemize}
				\item all with unnatural $J^P$
			\end{itemize}
		\end{itemize}
	\end{minipage}
	\begin{minipage}{0.49\textwidth}
		\begin{tikzpicture}[x=\textwidth,y=\textwidth]
			\node[inner sep=0pt, anchor=south west] at (0, 0)
			{\includegraphics[width=\textwidth]{ds}};
			\node(1)  [black, rounded rectangle, fill=white!30!blue,    anchor=west, minimum width=0.30\textwidth] at (0.10,-0.12) {\tiny $D_s^{\phantom{*}}$};
			\node(2)  [below=0.02 of 1,  black, rounded rectangle, fill=white!30!blue,  minimum width=0.30\textwidth] {\tiny $D_s^*$};
			\node(3)  [right=0.02 of 1,  black, rounded rectangle, fill=black!20!green, minimum width=0.30\textwidth] {\tiny $D_{s0}^*(2317)$};
			\node(4)  [below=0.02 of 3,  black, rounded rectangle, fill=black!20!green, minimum width=0.30\textwidth] {\tiny $D_{s1}^{\phantom{*}}(2460)$};
			\node(5)  [below=0.02 of 4,  black, rounded rectangle, fill=black!20!green, minimum width=0.30\textwidth] {\tiny $D_{s1}^{\phantom{*}}(2536)$};
			\node(6)  [below=0.02 of 5,  black, rounded rectangle, fill=black!20!green, minimum width=0.30\textwidth] {\tiny $D_{s2}^*(2573)$};
			\node(7)  [left= 0.02 of 6,  black, rounded rectangle, fill=white!30!blue,  minimum width=0.30\textwidth] {\tiny $D_{s1}^*(2700)$};
			\node(8)  [right=0.02 of 3,  black, rounded rectangle, fill=magenta,        minimum width=0.30\textwidth] {\tiny $D_{s1}^*(2860)$};
			\node(9)  [right=0.02 of 6,  black, rounded rectangle, fill=magenta,        minimum width=0.30\textwidth] {\tiny $D_{s3}^*(2860)$};
			\node(10) [below=0.05 of 6,  black, rounded rectangle, fill=red,            minimum width=0.30\textwidth] {\tiny $D_{sJ}^{\phantom{*}}(3040)$};

			\node(11)  [below=0.02 of 2,  draw, dashed, white!30!blue, rounded rectangle, minimum width=0.30\textwidth] {\phantom{\tiny $D_s^*$}};
			\node(12)  [below=0.02 of 8,  draw, dashed, magenta,       rounded rectangle, minimum width=0.30\textwidth] {\phantom{\tiny $D_s^*$}};
			\node(13)  [below=0.02 of 12, draw, dashed, magenta,       rounded rectangle, minimum width=0.30\textwidth] {\phantom{\tiny $D_s^*$}};

			\node[black, anchor=west] at (0.002, 0.01) {\tiny $J^P=$};
			\node[black, anchor=west] at (0.145, 0.01)  {\tiny $0^-$};
			\node[black, anchor=west] at (0.218, 0.01)  {\tiny $1^-$};
			\node[black, anchor=west] at (0.291, 0.01)  {\tiny $0^+$};
			\node[black, anchor=west] at (0.364, 0.01)  {\tiny $1^+$};
			\node[black, anchor=west] at (0.437, 0.01)  {\tiny $2^+$};
			\node[black, anchor=west] at (0.510, 0.01)  {\tiny $1^-$};
			\node[black, anchor=west] at (0.583, 0.01)  {\tiny $2^-$};
			\node[black, anchor=west] at (0.656, 0.01)  {\tiny $3^-$};
			\node[black, anchor=west] at (0.729, 0.01)  {\tiny $2^+$};
			\node[black, anchor=west] at (0.802, 0.01)  {\tiny $3^+$};
			\node[black, anchor=west] at (0.875, 0.01)  {\tiny $4^+$};
			\fill[blue, opacity=0.1]     (0.150,0.110) rectangle (0.298,0.645);
			\fill[green, opacity=0.1]    (0.298,0.110) rectangle (0.515,0.645);
			\fill[magenta, opacity=0.1]  (0.515,0.110) rectangle (0.735,0.645);
			\fill[yellow, opacity=0.1]   (0.735,0.110) rectangle (0.950,0.645);
			%\draw [step=0.05,gray, very thin] (0.,0.) grid (1.,0.7);
		\end{tikzpicture}
	\end{minipage}
	\end{center}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,overlay]
		%\draw [step=0.05,gray, very thin] (0.,0.) grid (1.,1.);
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth] at (0.800,0.740) {\tiny PRD 89 (2014) 074023};
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth] at (0.230,0.375) {\tiny PRL 90 (2003) 242001};
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth] at (0.230,0.190) {\tiny PRL 113 (2014) 162001};
	\end{tikzpicture}
\end{frame}

\begin{frame}{$\Ds$ Spectroscopy}
	\begin{center}
	\begin{minipage}{\textwidth}
		\begin{itemize}
			\item Inclusive analysis of $\proton\proton \to \Dstarp\KS X$ and $\Dstarz\Kp X$ decays
			\item Use $\Dstarp\to\Dz\pip,\,\Dz\to\Km\pip$ (shown) or $\Km\pip\pip\pim$ and $\Dstarz\to\Dz\piz\,,\Dz\to\Km\pip$ decay chains
			\item Builds on previous analyses of $\Dz\Kp$
			\item Access to natural (NP) and unnatural (UP) spin-parities
			\item Plots show (a) $\left|\cos\theta_H\right| < 0.5$ and (b) $> 0.5$ to emphasise NP and UP components
			\begin{itemize}
				\item Resonant contributions seen due to {\boldred $D_{s1}(2536)^+$}, {\boldgreen $D_{s2}^*(2573)^+$}, {\boldblue $D_{s1}^*(2700)^+$} and {\boldmagenta $D_{s3}^*(2860)^+$} resonances
				\item Weak evidence for structure around {\boldcyan 3\gevcc}
			\end{itemize}
		\end{itemize}
	\end{minipage}
	\begin{minipage}{0.65\textwidth}
		\begin{tikzpicture}[x=\textwidth,y=\textwidth]
			\node[inner sep=0pt, anchor=south west] at (0, 0)
			{\includegraphics[width=0.5\textwidth]{mDK_NP}};
			\node[inner sep=0pt, anchor=south west] at (0.5, 0)
			{\includegraphics[width=0.5\textwidth]{mDK_UP}};
			\fill[red, opacity=0.2]            (0.070,0.065) rectangle (0.090,0.410);
			\fill[black!20!green, opacity=0.2] (0.090,0.065) rectangle (0.110,0.410);
			\fill[blue, opacity=0.2]           (0.120,0.065) rectangle (0.200,0.410);
			\fill[magenta, opacity=0.2]        (0.200,0.065) rectangle (0.250,0.410);
			\fill[red, opacity=0.2]            (0.570,0.065) rectangle (0.590,0.410);
			\node[draw, dashed, thick, cyan, circle, minimum width=0.1\textwidth] at (0.9,0.33) {};
			%\draw [step=0.05,gray, very thin] (0.,0.) grid (1.,0.4);
		\end{tikzpicture}
	\end{minipage}
	\end{center}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,overlay,shift={(current page.south west)}]
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth] at (0.830,0.050) {\tiny JHEP 02 (2016) 133};
	\end{tikzpicture}
\end{frame}

\begin{frame}{$\Ds$ Spectroscopy}
	\begin{minipage}{\textwidth}
		\begin{itemize}
			\item {\color{dblue}NP} and {\color{dred}UP} resonances identified by helicity angle distribution of $\Dstar$ decay
			\item {\color{dblue}NP} resonances follow {\color{dblue}$\sin^2\theta_H$} distribution
			\item {\color{dred}UP} resonances follow {\color{dred}$1 + h\cos^2\theta_H$}
			\item Expected distributions seen for (a--c) {\color{dblue}$D_{s1}^*(2700)^+$}, {\color{dblue}$D_{s3}^*(2860)^+$} and {\color{dred}$D_{sJ}(3040)^+$}
			\item Data consistent with additional {\color{dred}UP} contribution in 2.86\gevcc region
		\end{itemize}
		\includegraphics[width=0.3\textwidth]{cosThetaH_2700}
		\includegraphics[width=0.3\textwidth]{cosThetaH_2860}
		\includegraphics[width=0.3\textwidth]{cosThetaH_3040}
	\end{minipage}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,overlay,shift={(current page.south west)}]
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth] at (0.830,-0.030) {\tiny JHEP 02 (2016) 133};
	\end{tikzpicture}
\end{frame}

\begin{frame}{$\Ds$ Spectroscopy}
	\begin{minipage}{\textwidth}
		\begin{itemize}
			\item First observation of $D_{s2}^*(2573)^+\to\Dstarp\KS$ decay
			\item Branching ratio relative to $D_{s2}^*(2573)^+\to\Dp\KS$ measured to be
			\begin{equation*}
				\frac{\mathcal{B}(D_{s2}^*(2573)^+\to\Dstarp\KS)}{\mathcal{B}(D_{s2}^*(2573)^+\to\Dp\KS)} = 0.044 \pm 0.005\,{(\rm stat.)} \pm 0.011\,{(\rm syst.)}
			\end{equation*}
		\end{itemize}
	\end{minipage}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,overlay,shift={(current page.south west)}]
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth] at (0.830,-0.030) {\tiny JHEP 02 (2016) 133};
	\end{tikzpicture}
\end{frame}

\begin{frame}{$\Dz$ Spectroscopy}
	\begin{center}
	\begin{minipage}{0.49\textwidth}
		\begin{itemize}
			\item $\Dz$ spectrum similar to $\Ds$
			\item Most recently studied in $\ep\en\to\DorDstarp\pim X$ and $\proton\antiproton\to\DorDstarp\pim X$ inclusive decays at BaBar and LHCb, respectively.
			\item May also be compared against $\Dp$ spectroscopy results 
			\item A number of claimed states have {\boldred unknown} $J^P$
			\item Dalitz plot analysis of $\Bp\to\Dm\pip\pip$ decays allows $J^P$ of natural states to be measured
		\end{itemize}
	\end{minipage}
	\begin{minipage}{0.49\textwidth}
		\begin{tikzpicture}[x=\textwidth,y=\textwidth]
			\node[inner sep=0pt, anchor=south west] at (0, 0)
			{\includegraphics[width=\textwidth]{du}};
			\node(1)  [black, rounded rectangle, fill=white!30!blue,    anchor=west, minimum width=0.30\textwidth] at (0.10,-0.12) {\tiny $D_{\phantom{0}}^{0}$};
			\node(2)  [below=0.02 of 1,  black, rounded rectangle, fill=white!30!blue,  minimum width=0.30\textwidth] {\tiny $D_{\phantom{0}}^{*0}$};
			\node(3)  [right=0.02 of 1,  black, rounded rectangle, fill=black!20!green, minimum width=0.30\textwidth] {\tiny $D_{0}^*(2400)_{\phantom{0}}^0$};
			\node(4)  [below=0.02 of 3,  black, rounded rectangle, fill=black!20!green, minimum width=0.30\textwidth] {\tiny $D_{1}(2420)_{\phantom{0}}^0$};
			\node(5)  [below=0.02 of 4,  black, rounded rectangle, fill=black!20!green, minimum width=0.30\textwidth] {\tiny $D_{1}(2430)_{\phantom{0}}^0$};
			\node(6)  [below=0.02 of 5,  black, rounded rectangle, fill=black!20!green, minimum width=0.30\textwidth] {\tiny $D_{2}^*(2460)_{\phantom{0}}^0$};
			\node(7)  [below=0.02 of 2,  black, rounded rectangle, fill=white!30!blue,  minimum width=0.30\textwidth] {\tiny $D(2550)_{\phantom{0}}^0$};
			\node(8)  [right=0.02 of 3,  black, rounded rectangle, fill=magenta,        minimum width=0.30\textwidth] {\tiny $D_{1}^*(2760)_{\phantom{0}}^0$};
			\node(9)  [below=0.05 of 6,  black, rounded rectangle, fill=red,            minimum width=0.30\textwidth] {\tiny $D_J^*(2650)_{\phantom{0}}^0$};
			\node(10) [left= 0.02 of 9,  black, rounded rectangle, fill=red,            minimum width=0.30\textwidth] {\tiny $D_J^*(2600)_{\phantom{0}}^0$};
			\node(11) [right=0.02 of 9,  black, rounded rectangle, fill=red,            minimum width=0.30\textwidth] {\tiny $D_J^*(2760)_{\phantom{0}}^0$};
			\node(12) [below=0.02 of 10, black, rounded rectangle, fill=red,            minimum width=0.30\textwidth] {\tiny $D_J(2740)_{\phantom{0}}^0$};
			\node(12) [below=0.02 of 9,  black, rounded rectangle, fill=red,            minimum width=0.30\textwidth] {\tiny $D_J(3000)_{\phantom{0}}^0$};

			\node(13) [below=0.02 of 7,  draw, dashed, white!30!blue, rounded rectangle, minimum width=0.30\textwidth] {\phantom{\tiny $D_0^0$}};
			\node(14) [below=0.02 of 8,  draw, dashed, magenta,       rounded rectangle, minimum width=0.30\textwidth] {\phantom{\tiny $D_0^0$}};
			\node(15) [below=0.02 of 14, draw, dashed, magenta,       rounded rectangle, minimum width=0.30\textwidth] {\phantom{\tiny $D_0^0$}};
			\node(16) [below=0.02 of 15, draw, dashed, magenta,       rounded rectangle, minimum width=0.30\textwidth] {\phantom{\tiny $D_0^0$}};

			\node[black, anchor=west] at (0.002, 0.01) {\tiny $J^P=$};
			\node[black, anchor=west] at (0.145, 0.01)  {\tiny $0^-$};
			\node[black, anchor=west] at (0.218, 0.01)  {\tiny $1^-$};
			\node[black, anchor=west] at (0.291, 0.01)  {\tiny $0^+$};
			\node[black, anchor=west] at (0.364, 0.01)  {\tiny $1^+$};
			\node[black, anchor=west] at (0.437, 0.01)  {\tiny $2^+$};
			\node[black, anchor=west] at (0.510, 0.01)  {\tiny $1^-$};
			\node[black, anchor=west] at (0.583, 0.01)  {\tiny $2^-$};
			\node[black, anchor=west] at (0.656, 0.01)  {\tiny $3^-$};
			\node[black, anchor=west] at (0.729, 0.01)  {\tiny $2^+$};
			\node[black, anchor=west] at (0.802, 0.01)  {\tiny $3^+$};
			\node[black, anchor=west] at (0.875, 0.01)  {\tiny $4^+$};
			\fill[blue, opacity=0.1]     (0.150,0.110) rectangle (0.298,0.645);
			\fill[green, opacity=0.1]    (0.298,0.110) rectangle (0.515,0.645);
			\fill[magenta, opacity=0.1]  (0.515,0.110) rectangle (0.735,0.645);
			\fill[yellow, opacity=0.1]   (0.735,0.110) rectangle (0.950,0.645);
			%\draw [step=0.05,gray, very thin] (0.,0.) grid (1.,0.7);
		\end{tikzpicture}
	\end{minipage}
	\end{center}
	\begin{tikzpicture}[x=\textwidth,y=\textwidth,overlay]
		%\draw [step=0.05,gray, very thin] (0.,0.) grid (1.,1.);
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth] at (0.800,0.780) {\tiny PRD 93 (2016) 034035};
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth, rotate=90] at (-0.050,0.480) {\tiny PRD 82 (2010) 111101};
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth, rotate=90] at (0.010,0.480) {\tiny JHEP 09 (2013) 145};
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth, rotate=90] at (-0.050,0.200) {\tiny PRD 92 (2015) 032002};
		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth, rotate=90] at (0.010,0.200) {\tiny PRD 92 (2015) 012012};
%		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth] at (0.230,0.375) {\tiny PRL 90 (2003) 242001};
%		\node [white, rounded rectangle, fill=orange, anchor=west, align=center, minimum width=0.28\textwidth] at (0.230,0.190) {\tiny PRL 113 (2014) 162001};
	\end{tikzpicture}
\end{frame}

\end{document}
